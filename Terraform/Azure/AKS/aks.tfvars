aks_name = "aks-sea-kanomnutt"
location = "southeastasia"
resource_group_name = "rg-sea-kanomnutt"
kubernetes_version = "1.27.9"
sku_tier = "Free"
image_cleaner_interval_hours = 24
azure_ad = {
  rbac_managed = true
  tenant_id = "ab983f65-a891-4847-ad81-8f3cc25c6d4b"
  admin_group = [ "15e4c34c-3d93-44b1-83c9-80bc3b17e7a8" ]
  rbac_enabled = false
  client_app_id = null
  server_app_id = null
  server_app_secret = null
}
vnet_rg = "rg-sea-kanomnutt"
aks_vnet_name = "vnet-sea-kanomnutt"
aks_subnet_name = "snet-aks-001"
storage_profile = {
  blob_driver_enabled = true
  disk_driver_enabled = true
  disk_driver_version = "v1"
  file_driver_enabled = true
  snapshot_controller_enabled = true
}
agent_pool = {
  name = "agt"
  vm_size = "Standard_D2_v2"
  os_sku = "Ubuntu"
  os_disk_size_gb = 128
  enabled_pip = false
  enabled_auto_scale = false
  node_count = 1
  max_pods = 40
  availability_zones = [ "2" ]
  only_critical_addons_enabled = true
}

node_pool_stf = { 
  name = "stf"
  vm_size = "Standard_D2_v2"
  enabled_auto_scale = false
  node_count = 1
  enabled_public_ip = false
  max_pods = 40
  mode = "User"
  availability_zones = [ "2" ]
  node_disk_size = 128
  node_os_sku = "Ubuntu"
  node_labels = {
    "NodePool" = "Stateful"
  }
  node_taints = [ "StatefulOnly=true:NoSchedule" ]
}

node_pool_stl = {
  name = "stl"
  vm_size = "Standard_D2_v2"
  enabled_auto_scale = true
  node_min_count = 1
  node_max_count = 10
  node_count = 1
  enabled_public_ip = false
  max_pods = 40
  mode = "User"
  availability_zones = [ "2" ]
  node_disk_size = 128
  node_os_sku = "Ubuntu"
  node_labels = {
    "NodePool" = "Stateful"
  }
  node_taints = [ "StatelessOnly=true:NoSchedule" ]
}

tags = {
  "CreateDate" = "12-Mar-2024"
}