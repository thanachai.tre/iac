provider "azurerm" {
  features {}
}

terraform {
  backend "azurerm" {
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "stseakanomnutttf"
    container_name = "tfstate"
    key = "vnet.tfstate"
  }
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.85.0"
    }
  }
}

module "vnet" {
  source = "../../../Terraform Module/Azure/VNET"
  name = var.name
  resource_group_name = var.resource_group_name
  location = var.location
  address_space = var.address_space
  subnet = var.subnet
  tags = var.tags
}