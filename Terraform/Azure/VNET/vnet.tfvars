name = "vnet-sea-kanomnutt"
resource_group_name = "rg-sea-kanomnutt"
location = "southeastasia"
address_space = [ "10.0.0.0/16", "192.168.0.0/16" ]
subnet = [ {
  name = "snet-sea-kanomnutt01"
  address_prefix = "192.168.1.0/24"
},
{
  name = "snet-sea-kanomnutt02"
  address_prefix = "10.0.10.0/24"
},
{
  name = "snet-sea-kanomnutt03"
  address_prefix = "10.0.1.0/24"
},
{
  name = "snet-sea-kanomnutt04"
  address_prefix = "192.168.3.0/24"
} ]
tags = {
  CreateDate = "28-Feb-2023"
  Automated = "Terraform"
}