ami = "ami-062550af7b9fa7d0" # Ubuntu 20.04
subnet_id = "subnet-0e5ce6436bfc82b7e"
associate_public_ip_address = true
vpc_security_group_ids = [ "sg-05187f76793eaacf4" ]
tags = {
  "name" = "Terraform-EC2"
}