# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version = "4.67.0"
  hashes = [
    "h1:LfOuBkdYCzQhtiRvVIxdP/KGJODa3cRsKjn8xKCTbVY=",
    "h1:dCRc4GqsyfqHEMjgtlM1EympBcgTmcTkWaJmtd91+KA=",
    "zh:0843017ecc24385f2b45f2c5fce79dc25b258e50d516877b3affee3bef34f060",
    "zh:19876066cfa60de91834ec569a6448dab8c2518b8a71b5ca870b2444febddac6",
    "zh:24995686b2ad88c1ffaa242e36eee791fc6070e6144f418048c4ce24d0ba5183",
    "zh:4a002990b9f4d6d225d82cb2fb8805789ffef791999ee5d9cb1fef579aeff8f1",
    "zh:559a2b5ace06b878c6de3ecf19b94fbae3512562f7a51e930674b16c2f606e29",
    "zh:6a07da13b86b9753b95d4d8218f6dae874cf34699bca1470d6effbb4dee7f4b7",
    "zh:768b3bfd126c3b77dc975c7c0e5db3207e4f9997cf41aa3385c63206242ba043",
    "zh:7be5177e698d4b547083cc738b977742d70ed68487ce6f49ecd0c94dbf9d1362",
    "zh:8b562a818915fb0d85959257095251a05c76f3467caa3ba95c583ba5fe043f9b",
    "zh:9b12af85486a96aedd8d7984b0ff811a4b42e3d88dad1a3fb4c0b580d04fa425",
    "zh:9c385d03a958b54e2afd5279cd8c7cbdd2d6ca5c7d6a333e61092331f38af7cf",
    "zh:b3ca45f2821a89af417787df8289cb4314b273d29555ad3b2a5ab98bb4816b3b",
    "zh:da3c317f1db2469615ab40aa6baba63b5643bae7110ff855277a1fb9d8eb4f2c",
    "zh:dc6430622a8dc5cdab359a8704aec81d3825ea1d305bbb3bbd032b1c6adfae0c",
    "zh:fac0d2ddeadf9ec53da87922f666e1e73a603a611c57bcbc4b86ac2821619b1d",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version = "3.60.0"
  hashes = [
    "h1:b8AJy2xJGS5t4ZLaYyojeRaPGAiCtWQ7mL9DKnhyHNc=",
    "zh:0ee99a2a59b8ec8db077ccbed88739e2c84e5c1dceae166171602e2001e08128",
    "zh:194636208067e5c24d077aa74e1405a757e70dff35a7618d768a748710be1eba",
    "zh:1a69482589e31834b38226762263bd0b8fc9f86b16403705d2e4d161e6f209e6",
    "zh:23fa6b45f2bb19c3218c1b47528e538456444c86b3bb542df02a4ba3b788e662",
    "zh:715b62b26ee81b07845613e32dc72899c694af91cbc1756b32783cab89836ffe",
    "zh:8bc1f72454e465c7ac8aaa87bf09dcb5c560fc8d2d976cb11b7f206ff4f2e0d0",
    "zh:8d4be7c813f3859b78caefcd11e88e9631155986b846bab94a98eadbc25d817e",
    "zh:aaf63f83ae41f4a3cb6f18d2d5fc403fa2047d5ad74ee2c4aa24db91a3ef851c",
    "zh:cb444a37d09683340d38629bfb33c925865eebe2ff1ca3f1d0ae470f6a82ab6b",
    "zh:ed2dae0151e1591799061711e9b63fee283f3e6f47829b2521baab5e04ab5b5e",
    "zh:f05e8e85a8f61ed1a7d3929fce1a529a9a2aebb2d59704994d4c9e80626aef5d",
    "zh:f569b65999264a9416862bca5cd2a6177d94ccb0424f3a4ef424428912b9cb3c",
  ]
}
