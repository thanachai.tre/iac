vpc_cidr = "192.168.0.0/16"
subnet_cidr = [ "192.168.1.0/24", "192.168.2.0/24", "192.168.3.0/24", "192.168.4.0/24", "192.168.5.0/24" ]
# subnet_cidr = [ "192.168.1.0/24" ]
tag_vpc = {
  "name" = "kanomnutt-vpc"
}
tag_gw = {
  "name" = "kanomnutt-gw"
}
tag_subnet = {
  "name" = "kanomnutt-subnet"
}
tag_sg = {
  "name" = "kanomnutt-sg"
}