variable "ecr_name" {
  type = string
}
variable "image_tag_mutability" {
  type = string
  default = "MUTABLE"
}
variable "image_scaning" {
  type = bool
  default = true
}
variable "tags" {
  type = map(string)
}