provider "aws" {
  region = "ap-southeast-1"
  profile = "default"
}

terraform {
  backend "azurerm" {
    # access_key = "$STORAGE_ACCOUNT_KEY"
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "terraformstatekanomnutt"
    container_name = "tfstate"
    key = "aws-ecr.tfstate"
  }
}

resource "aws_ecr_repository" "ecr" {
  name = var.ecr_name
  image_tag_mutability = var.image_tag_mutability
  force_delete = true
  image_scanning_configuration {
    scan_on_push = var.image_scaning
  }
  tags = var.tags
}
