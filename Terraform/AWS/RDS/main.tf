terraform {
  backend "azurerm" {
    # access_key = "$STORAGE_ACCOUNT_KEY"
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "terraformstatekanomnutt"
    container_name = "tfstate"
    key = "aws-rds.tfstate"
  }
}