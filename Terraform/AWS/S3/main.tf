provider "aws" {
  region = "ap-southeast-1"
  profile = "default"
}

terraform {
  backend "azurerm" {
    # access_key = "$STORAGE_ACCOUNT_KEY"
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "terraformstatekanomnutt"
    container_name = "tfstate"
    key = "aws-s3.tfstate"
  }
}

resource "aws_s3_bucket" "s3" {
  bucket = "kanomnutt-bucket"
  force_destroy = true
  object_lock_enabled = false
  tags = {
    "Name" = "kanomnutt-s3"
  }
}

resource "aws_s3_bucket_versioning" "versioning" {
  bucket = aws_s3_bucket.s3.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_ownership_controls" "example" {
  bucket = aws_s3_bucket.s3.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "s3_acl" {
  bucket = aws_s3_bucket.s3.id
  acl = "private"
  depends_on = [aws_s3_bucket_ownership_controls.example]
}