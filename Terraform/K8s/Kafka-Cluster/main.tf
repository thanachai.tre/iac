provider "kubernetes" {
  config_path = "~/.kube/config"
}

provider "kubectl" {
  config_path = "~/.kube/config"
}

terraform {
  backend "azurerm" {
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "terraformstatekanomnutt"
    container_name = "tfstate"
    key = "k8s-kafka-cluster.tfstate"
  }
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "= 2.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
  }
}

data "kubectl_path_documents" "kafka_file" {
    pattern = var.pattern
    vars = {
        namespaces = var.namespaces
    }
}

resource "kubectl_manifest" "kafka" {
    for_each  = toset(data.kubectl_path_documents.kafka_file.documents)
    yaml_body = each.value
}
