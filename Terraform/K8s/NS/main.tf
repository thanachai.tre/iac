terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.20.0"
    }
  }
  backend "azurerm" {
    resource_group_name = "rg-sea-kanomnutt"
    storage_account_name = "terraformstatekanomnutt"
    container_name = "tfstate"
    key = "k8s-ns.tfstate"
  }
}

provider "kubernetes" {
  config_path = "~/.kube/config"
  #config_context = "k8s.kanomnutt.site"
}

module "namespace" {
source = "./module"
#  source = "git@gitlab.com:thanachai.tre/terraform-module//k8s/ns"
  k8s_ns = var.k8s_ns
}

# resource "kubernetes_namespace" "k8s_ns" {
#   count = length(var.k8s_ns)
#   metadata {
#     name = var.k8s_ns[count.index].ns_name
#     labels = var.k8s_ns[count.index].ns_label
#   }
# }