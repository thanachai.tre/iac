sa_name = "kanomnutt-ns02"
namespace = "ns02"
name = "ro-kanomnutt-ns02"
labels = {
    CreateDate : "10-May-2023"
    System : "Terraform"
}
rules = [
  {
    api_groups = [""]
    resources = ["pods","configmaps","services","pods/exec","pods/log","pods/status","pods/event","pods/portforward","endpoints"]
    resource_names = []
    verbs = ["get","list","watch","describe","create","update","patch","delete","top"]
  },
  {
    api_groups = [""]
    resources = ["serviceaccounts","events","replicationcontrollers","secrets"]
    resource_names = []
    verbs = ["get","list","watch","describe"]
  },
  {
    api_groups = ["apps"]
    resources = ["deployments","replicasets"]																				
    resource_names = []
    verbs = ["get","list","watch","create","update","patch","delete","logs"]
  },	
  {
    api_groups = ["apps"]
    resources = ["statefulsets"]
    resource_names = []
    verbs = ["get","list","watch","describe","create","update","patch","delete"]
  },
  {
    api_groups = ["batch"]
    resources = ["cronjobs","jobs"]
    resource_names = []
    verbs = ["get","list","watch","describe","create","update","patch","delete"]
  },  
  {
    api_groups = ["autoscaling"]
    resources = ["horizontalpodautoscalers"]
    resource_names = []
    verbs = ["get","list","watch","describe","create","update","patch","delete"]
  },
  {
    api_groups = ["metrics.k8s.io"]
    resources = ["pods"]
    resource_names = []
    verbs = ["get","list","watch","top"]
  },
  {
    api_groups = ["networking.k8s.io","extensions"]
    resources = ["ingresses","ingresses/status"]
    resource_names = []
    verbs = ["get","list","watch"]
  },
  {
    api_groups = ["events.k8s.io",""]
    resources = ["events"]
    resource_names = []
    verbs = ["get","list","watch"]
  },
]
subject_kind = "ServiceAccount"
group_ids = ["kanomnutt-ns02"]