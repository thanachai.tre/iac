release_name = "node-cert-exporter" 
chart_repository = "https://charts.enix.io" 
chart_name = "x509-certificate-exporter" 
chart_version = "3.6.0" 
config_file_path = "values.yaml" 
namespace = "monitoring"