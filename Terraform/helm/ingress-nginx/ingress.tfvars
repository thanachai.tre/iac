release_name = "ingress-nginx" 
chart_repository = "https://kubernetes.github.io/ingress-nginx" 
chart_name = "ingress-nginx" 
chart_version = "4.6.0" 
config_file_path = "values.yaml" 
namespace = "ingress-nginx" 
lint_enabled = true 