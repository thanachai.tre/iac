release_name = "gitlab-runner" 
chart_repository = "https://charts.gitlab.io" 
chart_name = "gitlab-runner" 
chart_version = "0.52.0" 
config_file_path = "values.yaml" 
namespace = "gitlab" 
lint_enabled = true 