release_name = "kube-prometheus-stack" 
chart_repository = "https://prometheus-community.github.io/helm-charts" 
chart_name = "kube-prometheus-stack" 
chart_version = "41.4.1" 
config_file_path = "values.yaml" 
namespace = "monitoring" 
lint_enabled = false 