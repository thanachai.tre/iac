release_name = "strimzi" 
chart_repository = "https://strimzi.io/charts" 
chart_name = "strimzi-kafka-operator" 
chart_version = "0.35.1" 
config_file_path = "values.yaml" 
namespace = "kafka" 
lint_enabled = true